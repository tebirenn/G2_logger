import unittest
import g2_custom_logger.logger as logger_module


class TestLogger(unittest.TestCase):

    def test_logger_creation(self):
        logger = logger_module.create_logger()
        self.assertIsInstance(logger, logger_module.MyLogger)


    def test_logging_levels(self):
        logger = logger_module.create_logger()
        logger.debug("Это отладочное сообщение")
        logger.info("Это информационное сообщение")
        logger.warning("Это предупреждение")
        logger.error("Это сообщение об ошибке")
        logger.critical("Это критическая ошибка")


    def test_custom_formatting(self):
        logger = logger_module.create_logger()
        custom_message = "Это пользовательское сообщение"
        formatted_message = logger.custom_format(custom_message)


if __name__ == '__main__':
    unittest.main()
