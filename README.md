# G2 Custom Logger

**G2 Custom Logger** is a simple Python logging library that allows you to configure and use a custom logger in your Python projects.

## Installation

You can install G2 Custom Logger using pip:

```bash
pip install g2-custom-logger
```

## Usage
```bash
import g2_custom_logger.logger as logger_module
```

# Create a logger
```bash
logger = logger_module.create_logger()

# Log messages at different levels
logger.debug("This is a debug message")
logger.info("This is an informational message")
logger.warning("This is a warning")
logger.error("This is an error message")
logger.critical("This is a critical error")
```

# Custom message formatting
```bash
custom_message = "This is a custom message"
formatted_message = logger.custom_format(custom_message)
```

## Configuration
You can configure the logger by specifying a name and log level when creating the logger. For example:

# Create a logger with a custom name and log level
```bash
logger = logger_module.create_logger(name='my_logger', level=logging.DEBUG)
```

## Documentation
For additional information and documentation, please visit your <a href="https://pypi.org/project/g2-custom-logger/">documentation.</a>